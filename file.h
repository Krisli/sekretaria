//
// Created by krisli on 19-06-04.
//

#ifndef SEKRETARIA_FILE_H
#define SEKRETARIA_FILE_H

#include <stdbool.h>

bool createConnection(char fileName[], char operation);
void closeConnection();
void loadBuffer();
char* getBuffer();
void saveToFile(char input[]);

#endif //SEKRETARIA_FILE_H
