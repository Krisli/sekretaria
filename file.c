//
// Created by krisli on 19-06-04.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

FILE *fp;
char buffer[999];
char directory[] = "files/";
char path[255];

bool createConnection(char fileName[], char operation) {
    memset(&path[0], 0, sizeof(path));
    if (operation == ' ') {
        operation = 'r';
    }

    strcat(path, directory);
    strcat(path, fileName);

    fp = fopen(path, &operation);
    if (!fp) {
        return false;
    }

    return true;
}

void closeConnection() {
    fclose(fp);
}

void loadBuffer() {
    while(fscanf(fp, "%s", buffer) != EOF) {
    }
}

char * getBuffer() {
    char *tmp = strdup(buffer);
    memset(buffer, 0, 999);
    return tmp;
}

void saveToFile(char input[]) {
    fprintf(fp, "%s\n", input);
}