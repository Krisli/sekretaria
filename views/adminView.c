//
// Created by krisli on 19-06-04.
//

#include <stdio.h>
#include <string.h>

#include "models/sekretare.h"
#include "adminView.h"
#include "main.h"

void mainAdminView();
void logout();
void printoSekretaret();

void addSekretareView();
void deleteSekretareView();
void updateSekretareView();

void loginAdminView() {
    printf("\nMiresevini ne panelin e administratorit!\n");

    do {
        printf("\nJu lutem identifikohuni\n");
        printf("---------------------------------------------------\n");
    } while (!loginAdmin());

    mainAdminView();
}

void mainAdminView() {
    int input;

    printf("\n\nMiresevjen Administrator!\n");
    printf("-------------------------------------------------------------\n");
    printf("1. Shto llogari (sekretari)\n");
    printf("2. Fshi llogari (sekretari)\n");
    printf("3. Rivendos fjalekalim per nje llogari (sekretari)\n");
    printf("4. Gjenero listen emer mbiemer te llogarive (sekretari)\n");
    printf("5. Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 5) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 5: ");
        res = scanf("%d", &input);
    }

    switch (input) {
        case 1:
            addSekretareView();
            break;
        case 2:
            deleteSekretareView();
            break;
        case 3:
            updateSekretareView();
            break;
        case 4:
            printoSekretaret();
            mainAdminView();
            break;
        case 5:
            logout();
            break;
    }
}

void printoSekretaret() {
    Sekretare *sekretaret = getSekretaret();

    printf("\n~~~~~~~~~o Lista e Sekretareve o~~~~~~~~~\n");
    printf("-----------------------------------------\n");
    printf(" %-2s| %-10s| %-11s| %-10s|\n", "#", "EMER", "MBIEMER", "USERNAME");
    printf("-----------------------------------------\n");

    int i = 0;
    Sekretare sek = sekretaret[i];
    while (strcmp(sek.emer, "-1") != 0) {
        printf("%-3d| %-10s| %-11s| %-10s|\n", i+1, sek.emer, sek.mbiemer, sek.username);

        i++;
        sek = sekretaret[i];
    }

    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
}

void logout() {
    printf("Duke dale nga paneli i administrimit...\n");
    startApp();
}

void addSekretareView() {
    char emer[50];
    char mbiemer[50];
    char username[50];
    char fjalekalim[255];

    bool vazhdo = true;

    while (vazhdo) {
        printf("\nJu lutem fusni te dhenat e sekretares qe doni te shtoni:\n");
        printf("---------------------------------------------------------\n");
        printf("Emri: ");
        scanf("%s", emer);
        printf("Mbiemri: ");
        scanf("%s", mbiemer);
        printf("Username: ");
        scanf("%s", username);
        printf("Fjalekalimi: ");
        scanf("%s", fjalekalim);

        char input[2];
        printf("Deshironi te shtoni nje llogari tjeter sekretarie? [Y/N]\n");
        getchar();
        scanf("%s", input);
        addSekretare(emer, mbiemer, username, fjalekalim);

        if (strcmp(input, "Y") == 0 || strcmp(input, "y") == 0) {
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    mainAdminView();
}

void deleteSekretareView() {
    char username[50];

    printoSekretaret();
    printf("\n\nJu lutem emrin e llogarise se sekretares qe doni te fshini:\n");
    printf("--------------------------------------------------------------\n");
    printf("Username: ");

    scanf("%s", username);

    Sekretare sekretare = getSekretareByUsername(username);
    while (strcmp(sekretare.username, "error") == 0) {
        printf("\nKjo llogari nuk ekziston!\n");
        printf("Username: ");
        scanf("%s", username);
        sekretare = getSekretareByUsername(username);
    }

    char input[2];
    printf("Jeni i/e sigurt qe doni ta fshini kete llogari? [Y/N]\n");
    getchar();
    scanf("%s", input);

    if (strcmp(input, "Y") == 0 || strcmp(input, "y") == 0) {
        printf("Duke fshire llogarine e sekretarise me username %s ...\n", sekretare.username);
        deleteSekretare(sekretare.username);
        printf("Llogaria u fshi me sukses!\n");
    } else {
        printf("Fshirja u anullua...\n");
    }

    mainAdminView();
}

void updateSekretareView() {
    char emer[50];
    char mbiemer[50];
    char fjalekalim[255];

    printf("\nJu lutem fusni te dhenat e llogarise qe doni te ndryshoni:\n");
    printf("-----------------------------------------------------------\n");
    printf("Emri: ");
    scanf("%s", emer);
    printf("Mbiemri: ");
    scanf("%s", mbiemer);

    Sekretare sekretare = getSekretareByNameAndSurname(emer, mbiemer);
    while (strcmp(sekretare.username, "error") == 0) {
        printf("\nKjo llogari nuk ekziston!\n");
        printf("Emri: ");
        scanf("%s", emer);
        printf("Mbiemri: ");
        scanf("%s", mbiemer);
        sekretare = getSekretareByNameAndSurname(emer, mbiemer);
    }

    printf("\nFusni fjalekalimin e ri: ");
    scanf("%s", fjalekalim);
    strcpy(sekretare.fjalekalim, fjalekalim);

    if (!updateSekretare(sekretare)) {
        printf("\nNdodhi nje gabim gjate ndryshimit!\n");
    } else {
        printf("\nNdryshimet u aplikuan me sukses\n");
    }

    mainAdminView();
}