//
// Created by krisli on 19-06-05.
//

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "models/sekretare.h"
#include "models/lende.h"
#include "models/klase.h"
#include "models/student.h"
#include "sekretareView.h"

void logoutSekretare();
void mainSekretareView();
void printoStudentet();
void printoKlasat();
void printoLendet();

void addStudentView();
void updateStudentView();
void deleteStudentView();

void addLendeView();
void updateLendeView();

void addNoteView();
void updateNoteView(Student student);

void addKlaseView();

Sekretare sekretare;

void loginSekretareView() {
    printf("\nMiresevini ne panelin e sekretarit!\n");

    do {
        printf("\nJu lutem identifikohuni\n");
        printf("---------------------------------------------------\n");
    } while (!loginSekretare());

    sekretare = getSessionSekretare();
    mainSekretareView();
}

void mainSekretareView() {
    int input;

    printf("\n\nMiresevjen %s!\n", sekretare.username);
    printf("-------------------------------------------------------------\n");
    printf("1.  Shto student\n");
    printf("2.  Ndrysho student\n");
    printf("3.  Fshi student\n");
    printf("4.  Shto lende\n");
    printf("5.  Ndrysho lende\n");
    printf("6.  Shto note\n");
    printf("7.  Shto klase\n");
    printf("8.  Gjenero flete notash per student\n");
    printf("9.  Gjenero flete notash per lende\n");
    printf("10. Raporte\n");
    printf("11. Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 11) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 11: ");
        res = scanf("%d", &input);
    }

    // TODO add actions for each input
    switch (input) {
        case 1:
            addStudentView();
            break;
        case 2:
            updateStudentView();
            break;
        case 3:
            deleteStudentView();
            break;
        case 4:
            addLendeView();
            break;
        case 5:
            updateLendeView();
            break;
        case 6:
            addNoteView();
            break;
        case 7:
            addKlaseView();
            break;
        case 8:
            break;
        case 9:
            break;
        case 10:
            break;
        case 11:
            logoutSekretare();
            break;
    }
}

void logoutSekretare() {
    printf("Duke dale nga paneli i sekretarise...\n");
    startApp();
}

void printoStudentet() {
    Student *studentet = getStudentet();

    printf("\n~~~~~~~~~~~~~~o Lista e Studenteve o~~~~~~~~~~~~~~\n");
    printf("--------------------------------------------------\n");
    printf("%-3s| %-12s| %-10s| %-9s| %-7s|\n", "#", "ID","EMER", "MBIEMER", "KLASA");
    printf("--------------------------------------------------\n");

    int i = 0;
    Student stu = studentet[i];
    while (strcmp(stu.emer, "-1") != 0) {
        char klasId[20];
        if (strcmp(stu.klasa.id, "error") == 0 || strcmp(stu.klasa.id, "-1") == 0 ) {
            strcpy(klasId, "N/A");
        } else {
            strcpy(klasId, stu.klasa.id);
        }

        printf("%-3d| %-12d| %-10s| %-9s| %-7s|\n", i+1, stu.id, stu.emer, stu.mbiemer, klasId);

        i++;
        stu = studentet[i];
    }

    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
}

void printoKlasat() {
    Klase *klasat = getKlasat();

    printf("\n~~~~o Lista e Klasave o~~~~\n");
    printf("--------------------------\n");
    printf("%-3s| %-5s| %-5s| %-6s|\n", "#", "DEGA", "VITI", "GRUPI");
    printf("--------------------------\n");

    int i = 0;
    Klase klas = klasat[i];
    while (strcmp(klas.id, "-1") != 0) {
        printf("%-3d| %-5s| %-5d| %-6c|\n", i+1, klas.dega, klas.viti, klas.grupi);

        i++;
        klas = klasat[i];
    }

    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~");
}

void printoLendet() {
    Lende *lendet = getLendet();

    printf("\n~~~~~~~~~~~~~~o Lista e Lendeve o~~~~~~~~~~~~~~\n");
    printf("-----------------------------------------------\n");
    printf("%-3s| %-5s| %-20s| %-12s|\n", "#", "ID", "LENDA", "NR. STUDENT");
    printf("-----------------------------------------------\n");

    int i = 0;
    Lende lende = lendet[i];
    while (lende.id != -1) {
        printf("%-3d| %-4d | %-20s| %-12d|\n", i+1, lende.id, lende.emer, lende.nr_student);

        i++;
        lende = lendet[i];
    }

    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
}

void printoListeNotash(Student student) {
    printf("\nLista e notave per %s %s [%d]\n", student.emer, student.mbiemer, student.id);
    printf("----------------------------------------------\n");
    printf("%-3s| %-30s| %-8s|\n", "#", "LENDA", "NOTA");
    printf("----------------------------------------------\n");

    int i = 0;
    while (student.notat[i].lenda_id != -1) {
        Lende lende = getLende(student.notat[i].lenda_id);
        printf("%-3d| [%-5d] %-22s| %-8.2f|\n", i+1, lende.id, lende.emer, student.notat[i].nota);

        i++;
    }
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
}

void addStudentView() {
    char emer[50];
    char mbiemer[50];
    char fjalekalim[255];
    float notat[10];
    int lendet[10];

    bool vazhdo = true;

    while (vazhdo) {
        printf("\nJu lutem fusni te dhenat e studentit qe doni te shtoni:\n");
        printf("--------------------------------------------------------\n");
        printf("Emri: ");
        scanf("%s", emer);
        printf("Mbiemri: ");
        scanf("%s", mbiemer);
        printf("Fjalekalimi: ");
        scanf("%s", fjalekalim);

        char input[2];
        printf("Deshironi te shtoni nje student tjeter? [Y/N]\n");
        getchar();
        scanf("%s", input);
        addStudent(emer, mbiemer, fjalekalim, "-1", notat, lendet, 0);

        if (strcmp(input, "Y") == 0 || strcmp(input, "y") == 0) {
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    mainSekretareView();
}

void updateStudentView() {
    int id;

    printoStudentet();
    printf("\n\nJu lutem fusni ID e studentit qe doni te ndryshoni:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &id);
    while (res != 1 || id < 0) {
        getchar();
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &id);
    }

    Student student = getStudentById(id);
    while (strcmp(student.emer, "error") == 0) {
        printf("\nKy student nuk ekziston!\n");
        printf("ID: ");
        res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &id);
        }
        student = getStudentById(id);
    }

    printf("\nStudenti: [%d] %s %s\n", student.id, student.emer, student.mbiemer);

    printf("-------------------------------------------------------------\n");
    printf("1. Ndrysho emrin dhe mbiemrin\n");
    printf("2. Ndrysho fjalekalimin\n");
    printf("3. Ndrysho klasen\n");
    printf("4. Ndrysho noten\n");
    printf("5. Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int input;
    res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 5) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 5: ");
        res = scanf("%d", &input);
    }

    char new_emer[50];
    char new_mbiemer[50];
    char new_passw[255];

    switch (input) {
        case 1:
            printf("\nEmri i vjeter: %s\n", student.emer);
            printf("Emri i ri: ");
            scanf("%s", new_emer);
            strcpy(student.emer, new_emer);

            printf("\nMbiemri i vjeter: %s\n", student.mbiemer);
            printf("Mbiemri i ri: ");
            scanf("%s", new_mbiemer);
            strcpy(student.mbiemer, new_mbiemer);

            if (!updateStudent(student)) {
                printf("\nNdodhi nje gabim gjate ndryshimit!\n");
            } else {
                printf("\nNdryshimet u aplikuan me sukses\n");
            }
            break;
        case 2:
            printf("\nFusni fjalekalimin e ri: ");
            scanf("%s", new_passw);
            strcpy(student.fjalekalim, new_passw);

            if (!updateStudent(student)) {
                printf("\nNdodhi nje gabim gjate ndryshimit!\n");
            } else {
                printf("\nNdryshimet u aplikuan me sukses\n");
            }
            break;
        case 3:
            printoKlasat();

            char klasId[20];
            strcpy(klasId, student.klasa.id);
            char *teksti = "Studenti aktualisht nuk ndodhet ne asnje klase.";
            if (strcmp(klasId, "error") != 0) {
                teksti = klasId;
            }

            printf("\n\nKlasa e studentit: %s\n", teksti);

            printf("\nJu lutem fusni ID e klases: ");
            scanf("%s", klasId);

            Klase klasa = getKlase(klasId);
            while (strcmp(klasa.id, "error") == 0) {
                printf("\nKjo klase nuk ekziston!\n");
                printf("ID e klases: ");
                res = scanf("%s", klasId);
                klasa = getKlase(klasId);
            }

            student.klasa = klasa;
            if (!updateStudent(student)) {
                printf("\nNdodhi nje gabim gjate ndryshimit!\n");
            } else {
                printf("\nNdryshimet u aplikuan me sukses\n");
            }
            break;
        case 4:
            updateNoteView(student);
            break;
        case 5:
            printf("\nDuke u rikthyer ne menune kryesore...\n");
            mainSekretareView();
            break;
    }

    mainSekretareView();
}

void deleteStudentView() {
    int id;

    printoStudentet();
    printf("\n\nJu lutem fusni ID e studentit qe doni te fshini:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &id);
    while (res != 1 || id < 0) {
        getchar();
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &id);
    }

    Student student = getStudentById(id);
    while (strcmp(student.emer, "error") == 0) {
        printf("\nKy student nuk ekziston!\n");
        printf("ID: ");
        res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &id);
        }
        student = getStudentById(id);
    }

    printf("Duke fshire studentin %s me ID: %d\n", student.emer, student.id);
    deleteStudent(student.id);
    printf("Studenti u fshi me sukses!\n");

    mainSekretareView();
}

void addLendeView() {
    int id;
    char emer[100];

    printoLendet();
    printf("\n\nJu lutem fusni ID unike per lenden qe doni te shtoni:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &id);
    while (res != 1 || id < 0) {
        getchar();
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &id);
    }

    Lende lende = getLende(id);
    while (strcmp(lende.emer, "error") != 0) {
        printf("\nNje lende me kete ID ekziston!\n");
        printf("ID: ");
        res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &id);
        }
        lende = getLende(id);
    }

    printf("Emri i lendes: ");
    scanf("%s", emer);

    addLende(id, emer);

    printf("Duke shtuar lenden...\n");
    printf("Lenda u shtua me sukses!\n");

    mainSekretareView();
}

void updateLendeView() {
    int id;

    printoLendet();
    printf("\n\nJu lutem fusni ID e lendes qe doni te ndryshoni:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &id);
    while (res != 1 || id < 0) {
        getchar();
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &id);
    }

    Lende lende = getLende(id);
    while (strcmp(lende.emer, "error") == 0) {
        printf("\nNuk ekziston lende me kete ID!\n");
        printf("ID: ");
        res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &id);
        }
        lende = getLende(id);
    }

    printf("\nLenda: [%d] %s\n", lende.id, lende.emer);

    printf("-------------------------------------------------------------\n");
    printf("1. Ndrysho emrin e lendes\n");
    printf("2. Ndrysho degen e lendes\n");
    printf("3. Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int input;
    res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 3) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 3: ");
        res = scanf("%d", &input);
    }

    char new_emer[100];

    switch(input) {
        case 1:
            printf("\nEmri i vjeter: %s\n", lende.emer);
            printf("Emri i ri: ");
            scanf("%s", new_emer);
            strcpy(lende.emer, new_emer);

            if (!updateLende(lende)) {
                printf("\nNdodhi nje gabim gjate ndryshimit!\n");
            } else {
                printf("\nNdryshimet u aplikuan me sukses\n");
            }
            break;
        case 2:
            //TODO add lende selection process
            break;
        case 3:
            mainSekretareView();
            break;
    }

    mainSekretareView();
}

void addNoteView() {
    int studentId, lendeId;

    printoStudentet();
    printf("\n\nJu lutem fusni ID e studentit:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &studentId);
    while (res != 1 || studentId < 0) {
        getchar();
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &studentId);
    }

    Student student = getStudentById(studentId);
    while (strcmp(student.emer, "error") == 0) {
        printf("\nKy student nuk ekziston!\n");
        printf("ID: ");
        res = scanf("%d", &studentId);
        while (res != 1 || studentId < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &studentId);
        }
        student = getStudentById(studentId);
    }

    printoLendet();
    printf("\n\nJu lutem fusni ID e lendes:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    res = scanf("%d", &lendeId);
    while (res != 1 || lendeId < 0) {
        getchar();
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &lendeId);
    }

    Lende lende = getLende(lendeId);
    while (strcmp(lende.emer, "error") == 0) {
        printf("\nNuk ekziston lende me kete ID!\n");
        printf("ID: ");
        res = scanf("%d", &lendeId);
        while (res != 1 || lendeId < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &lendeId);
        }
        lende = getLende(lendeId);
    }

    Note new_note;
    new_note.lenda_id = lende.id;

    float nota;

    printf("\n\nJu lutem fusni noten per studentin %s %s ne lenden %s:\n", student.emer, student.mbiemer, lende.emer);
    printf("----------------------------------------------------\n");
    printf("Nota: ");

    res = scanf("%f", &nota);
    while (res != 1 || nota < 0 || nota > 10) {
        getchar();
        printf("Nota duhet te jete nje numer pozitiv me i vogel se 10!\n");
        printf("Nota: ");
        res = scanf("%f", &nota);
    }

    new_note.nota = nota;
    student.notat = addToNotat(new_note, student.notat);

    if (!updateStudent(student)) {
        printf("\nNdodhi nje gabim gjate ndryshimit!\n");
    } else {
        printf("\nNdryshimet u aplikuan me sukses\n");
    }

    mainSekretareView();
}

void updateNoteView(Student student) {
    printoListeNotash(student);
    int lendeId;

    printf("\n\nJu lutem fusni ID e lendes:\n");
    printf("----------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &lendeId);
    while (res != 1 || lendeId < 0) {
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID ");
        res = scanf("%d", &lendeId);
    }

    Lende lende = getLende(lendeId);
    while (strcmp(lende.emer, "error") == 0 || !isLendePresent(lende, student)) {
        printf("\nNuk ekziston lende me kete ID per kete student!\n");
        printf("ID: ");
        res = scanf("%d", &lendeId);
        while (res != 1 || lendeId < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID ");
            res = scanf("%d", &lendeId);
        }
        lende = getLende(lendeId);
    }

    Note new_note;
    new_note.lenda_id = lende.id;

    float nota;

    printf("\n\nJu lutem fusni noten per studentin %s %s ne lenden %s:\n", student.emer, student.mbiemer, lende.emer);
    printf("----------------------------------------------------\n");
    printf("Nota: ");

    res = scanf("%f", &nota);
    while (res != 1 || nota < 0 || nota > 10) {
        getchar();
        printf("Nota duhet te jete nje numer pozitiv me i vogel se 10!\n");
        printf("Nota: ");
        res = scanf("%f", &nota);
    }

    printf("Duke ndryshuar noten...\n");
    int i = 0;
    while (student.notat[i].lenda_id != -1) {
        if (student.notat[i].lenda_id == new_note.lenda_id) {
            student.notat[i].nota = nota;
            break;
        }
    }

    if (!updateStudent(student)) {
        printf("\nNdodhi nje gabim gjate ndryshimit!\n");
    } else {
        printf("\nNdryshimet u aplikuan me sukses\n");
    }

    mainSekretareView();
}

void addKlaseView() {
    printoKlasat();

    char dega[50];
    int viti;
    char grupi;

    bool vazhdo = true;

    while (vazhdo) {
        printf("\nJu lutem fusni te dhenat e klases qe doni te shtoni:\n");
        printf("-----------------------------------------------------\n");
        printf("Dega: ");
        scanf("%s", dega);

        printf("Viti: ");
        int res = scanf("%d", &viti);
        while (res != 1 || viti < 0 || viti > 6) {
            printf("Viti duhet te jete nje numer pozitiv me i vogel se 6!\n");
            printf("Viti: ");
            res = scanf("%d", &viti);
        }

        printf("Grupi: ");
        getchar();
        scanf("%c", &grupi);

        char input[2];
        printf("Deshironi te shtoni nje klase tjeter? [Y/N]\n");
        getchar();
        scanf("%s", input);

        addKlase(dega, viti, grupi);

        if (strcmp(input, "Y") == 0 || strcmp(input, "y") == 0) {
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    mainSekretareView();

}