//
// Created by krisli on 19-06-10.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "main.h"
#include "studentView.h"
#include "models/student.h"

void logoutStudent();
void mainStudentView();

Student student;

void loginStudentView() {
    printf("\nMiresevini ne panelin e studentit!\n");

    do {
        printf("\nJu lutem identifikohuni\n");
        printf("---------------------------------------------------\n");
    } while (!loginStudent());

    student = getSessionStudent();
    mainStudentView();
}

void mainStudentView() {
    int input;

    printf("\n\nMiresevjen %s %s!\n", student.emer, student.mbiemer);
    printf("-------------------------------------------------------------\n");
    printf("1.  Ndrysho fjalekalimin\n");
    printf("2.  Regjistrohu ne nje lende\n");
    printf("3.  Shfaq listen e lendeve\n");
    printf("4.  Shfaq listen e notave\n");
    printf("5.  Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 5) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 5: ");
        res = scanf("%d", &input);
    }

    // TODO add actions for each input
    switch (input) {
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            logoutStudent();
            break;
    }
}

void logoutStudent() {
    printf("Duke dale nga paneli i studentit...\n");
    startApp();
}