//
// Created by krisli on 19-06-04.
//

#ifndef SEKRETARIA_FUNCTIONS_H
#define SEKRETARIA_FUNCTIONS_H

int parseInt(char* str, int n);
float parseFloat(char* str);
char *strsep(char **stringp, const char *delim);
char *strReplace(char* str, char find, char replace);

#endif //SEKRETARIA_FUNCTIONS_H
