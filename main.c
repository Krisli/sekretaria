//
// Created by krisli on 19-06-04.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file.h"

#include "views/studentView.h"
#include "views/sekretareView.h"
#include "views/adminView.h"

bool login(int userType);
void startApp();
void exitApp();

void landingView();
bool chooseUserTypeView();

int main() {

    startApp();

    return 0;
}

void startApp() {
    landingView();
    chooseUserTypeView();
}

void exitApp() {
    printf("\nJu faleminderit qe perdoret sistemin tone te menaxhimit te sekretarise mesimore!\n");
    printf("Programi po mbyllet...\n");

    exit(0);
}

void landingView() {
    printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("              ____       _                    _  __              _ \n"
           "             |  _ \\  ___| |_ _   _ _ __ ___  | |/ /   _ _ __ ___(_)\n"
           "             | | | |/ _ \\ __| | | | '__/ _ \\ | ' / | | | '__/ __| |\n"
           "             | |_| |  __/ |_| |_| | | |  __/ | . \\ |_| | |  \\__ \\ |\n"
           "             |____/ \\___|\\__|\\__, |_|  \\___| |_|\\_\\__,_|_|  |___/_|\n"
           "                             |___/                                 ");
    printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("\n");

    printf("Miresevini ne sistemin e menaxhimit te sekretarise mesimore!\n\n");
}

bool chooseUserTypeView() {
    printf("Hyr ne sistem me te drejta:\n");
    printf("---------------------------\n");
    printf("1. Administrator\n");
    printf("2. Sekretar\n");
    printf("3. Student\n");
    printf("4. Dil nga programi\n");

    int input;
    int res = scanf("%d", &input);

    while (res != 1 || input < 1 || input > 4) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 4: ");
        res = scanf("%d", &input);
    }

    if (input != 4) {
        login(input);
    } else {
        exitApp();
    }
}

bool login(int userType) {
    switch (userType) {
        case 1:
            loginAdminView();
            break;
        case 2:
            loginSekretareView();
            break;
        case 3:
            loginStudentView();
            break;
    }
}