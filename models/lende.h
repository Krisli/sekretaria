//
// Created by krisl on 6/9/2019.
//

#ifndef SEKRETARIA_LENDE_H
#define SEKRETARIA_LENDE_H

typedef struct Lende {
    int id;
    char emer[100];
    int nr_student;
    char **klasat_ids;
    int *studentet_ids;
} Lende;

Lende getLende(int ID);
Lende *getLendet();
void addLende(int ID, char *emer);
bool updateLende(Lende lende);

#endif //SEKRETARIA_LENDE_H
