//
// Created by krisli on 19-06-05.
//

#ifndef SEKRETARIA_KLASE_H
#define SEKRETARIA_KLASE_H

typedef struct Klase {
    char id[20];
    char dega[50];
    int viti;
    char grupi;
} Klase;

Klase getKlase(char *ID);
Klase *getKlasat();
void addKlase(char *dega, int viti, char grupi);

#endif //SEKRETARIA_KLASE_H
