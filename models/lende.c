//
// Created by krisl on 6/9/2019.
//

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <file.h>
#include <functions.h>

#include "lende.h"

char *bLenda;                       // stringa me te dhenat e paperpunuara nga skedari
int lastLendId;                     // ID e elementit te fundit ne vektorin e lendeve
bool isLdBufferLoaded = false;      // percakton nqs e kemi lexuar skedarin apo jo

Lende lendet[999];                  // vektor strukture Lende

bool saveLendet();
int *parseStudentIds(char *data);
char **parseKlasatIds(char *data);

/**
 * Shton nje strukture te tipit Lende ne nje indeks te caktuar te vektorit lendet.
 *
 * @param lende
 * @param indexaddLendeToList
 */
void addLendeToList(Lende lende, int index) {
    lendet[index] = lende;
    lastLendId = index;
}

/**
 * Krijon lidhjen me skedarin 'lendet.txt', lexon te dhenat dhe i perpunon
 * duke krijuar nje strukture te tipit Lende per cdo te dhene ne skedar.
 *
 * @return bool
 */
void loadLendet() {
    char *fLendet;
    char *fFields;

    createConnection("lende.txt", 'r');
    loadBuffer();
    closeConnection();
    bLenda = getBuffer();

    char *file = strdup(bLenda);
    file = strtok(file, "#");

    Lende tmp;
    int field = 0;
    int count = 0;
    while ((fLendet = strsep(&file, "|"))) {
        if (strlen(fLendet) > 0) {
            while ((fFields = strsep(&fLendet, "~"))) {
                if (strlen(fFields) > 0) {
                    switch (field) {
                        case 0:
                            tmp.id = parseInt(fFields, strlen(fFields));
                            break;
                        case 1:
                            strcpy(tmp.emer, fFields);
                            break;
                        case 2:
                            tmp.nr_student = parseInt(fFields, strlen(fFields));
                            break;
                        case 3:
                            tmp.klasat_ids = parseKlasatIds(fFields);
                            break;
                        case 4:
                            tmp.studentet_ids = parseStudentIds(fFields);
                            break;
                    }
                    field++;
                }
            }
            field = 0;
            addLendeToList(tmp, count);
            count++;
        }
    }

    Lende last;
    last.id = -1;

    addLendeToList(last, count);
    isLdBufferLoaded = true;
}

/**
 * Merr si parameter stringen e paperpunuar qe permban ID-t e klasave. E perpunon sipas formatit dhe
 * kthen mbrapsh nje array me stringa qe permbajne ID klasash(**char).
 *
 * @param data
 * @return
 */
char **parseKlasatIds(char *data) {
    char *fFields;
    char lendet_ids[999][999];

    // nqs nuk ka klasa, kthe mbrapsh nje array me -1
    if (strcmp(data, "-1") == 0) {
        char *klasat_ids[1];
        klasat_ids[0] = "-1";
        return klasat_ids;
    }

    int field = 0;
    int count = 0;
    int largest_string = 0;
    while ((fFields = strsep(&data, "/"))) {
        if (strlen(fFields) > 0) {
            strcpy(lendet_ids[count], fFields);

            if (largest_string < (strlen(lendet_ids[count] + 1))) {
                largest_string = strlen(lendet_ids[count] + 1);
            }
            count++;
            field++;
        }
    }

    char **result;
    result = malloc(count * sizeof(char));

    strcpy(lendet_ids[count], "-1");

    int i = 0;
    while (strcmp(lendet_ids[i], "-1") != 0) {
        int length = strlen(lendet_ids[i]) + 1;
        result[i] = malloc(length * sizeof(char));
        strcpy(result[i], lendet_ids[i]);
        i++;
    }

    result[i] = malloc(3 * sizeof(char));
    strcpy(result[i], "-1");

    return result;
}

/**
 * Merr si parameter tektstin e paperpunuar me ID-t e studenteve te ndare me "/" ne mes. I perpunon
 * dhe kthen mbrapsh nje array me int (student ids).
 *
 * @param data
 * @return
 */
int *parseStudentIds(char *data) {
    char *fFields;
    int student_ids[999];

    // nqs ska studente, kthe mbrapsh nje array me 1 element -1
    if (strcmp(data, "-1") == 0) {
        int student_ids[1];
        student_ids[0] = -1;
        return student_ids;
    }

    int field = 0;
    int count = 0;
    while ((fFields = strsep(&data, "/"))) {
        if (strlen(fFields) > 0) {
            student_ids[count] = parseInt(fFields, strlen(fFields));;

            count++;
            field++;
        }
    }

    student_ids[count] = -1;

    return student_ids;
}

/**
 * Kerkon ne vektorin lendet per lenden me ID-n e dhene.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku ID eshte 'error'.
 *
 * @param ID
 * @return
 */
Lende getLende(int ID) {
    Lende tmp;

    if (!isLdBufferLoaded) {
        loadLendet();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bLenda) == 0) {
        strcpy(tmp.emer, "error");
        return tmp;
    }

    int i = 0;
    Lende lend = lendet[i];

    // itero deri ne fund te vektorit me lende (shenuar me -1)
    while (lend.id != -1) {
        // nqs gjendet lende me ID-n e dhene
        if (lend.id == ID) {
            return lend;
        }

        i++;
        lend = lendet[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje lende me ID e dhene
    strcpy(tmp.emer, "error");

    return tmp;
}

/**
 * Gjen indeksin e nje lende ne vektorin e lendeve me ane te ID.
 * Nqs vektori eshte bosh ose ID nuk gjendet, kthen -1.
 *
 * @param ID
 * @return indeksin nqs gjendet, -1 ne te kundert
 */
int getLendeIndex(int ID) {
    if (!isLdBufferLoaded) {
        loadLendet();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bLenda) == 0) {
        return -1;
    }

    bool found = false;

    int i = 0;
    Lende lend = lendet[i];

    while (lend.id != -1) {
        if (lend.id == ID) {
            found = true;
            break;
        }

        i++;
        lend = lendet[i];
    }

    if (found) {
        return i;
    } else {
        return -1;
    }
}

/**
 * Shton nje strukture te tipit Lende ne fund te vektorit lendet, bazuar ne
 * te dhenat qe i kalohen funksionit.
 *
 * @param ID
 * @param emer
 */
void addLende(int ID, char *emer) {
    Lende lende;
    lende.id = ID;
    lende.nr_student = 0;
    strcpy(lende.emer, emer);

    lende.studentet_ids = malloc(sizeof(int));
    lende.studentet_ids[0] = -1;

    lende.klasat_ids = malloc(sizeof(char) * 3);
    lende.klasat_ids[0] = "-1";

    // sigurohu qe te dhenat te jene ngarkuar nga skedari
    if (!isLdBufferLoaded) {
        loadLendet();
    }

    addLendeToList(lende, lastLendId);

    // shto elementin e fundit me -1
    lende.id = -1;
    addLendeToList(lende, ++lastLendId);

    saveLendet();
}

/**
 * Perkthen vektorin lendet ne formatin e skedarit, dhe ruan
 * te dhenat te 'lende.txt'.
 *
 * @return
 */
bool saveLendet() {
    char result[999] = "";

    int i = 0;
    Lende lende = lendet[i];
    while (lende.id != -1) {
        char tmp[255];
        sprintf(tmp, "%d", lende.id);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        strcat(result, lende.emer);
        strcat(result, "~");

        // ruaj vektorin student ids te ndare me "/"
        int j = 0;
        char tmpStudent[255], tmpp[255];
        while (lende.studentet_ids[j] != -1) {
            sprintf(tmpp, "%d", lende.studentet_ids[j]);

            if (lende.studentet_ids[j+1] != -1) {
                strcat(tmpp, "/");
            }

            strcat(tmpStudent, tmpp);
            j++;
            memset(&tmpp[0], 0, sizeof(tmpp));
        }

        // j permban numrin e ID-ve te studenteve
        lende.nr_student = j;
        sprintf(tmp, "%d", lende.nr_student);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");

        j = 0;
        while (strcmp(lende.klasat_ids[j], "-1") != 0) {
            sprintf(tmpp, "%s", lende.klasat_ids[j]);

            if (strcmp(lende.klasat_ids[j+1], "-1")) {
                strcat(tmpp, "/");
            }

            strcat(result, tmpp);
            j++;
            memset(&tmpp[0], 0, sizeof(tmpp));
        }

        // nqs nuk ka fare lende
        if (j == 0) {
            strcat(result ,"-1");
        }

        strcat(result, "~");

        // nqs nuk ka fare studente
        if (j == 0) {
            strcat(result, "-1");
        } else {
            strcat(result, tmpStudent);
        }

        strcat(result, "|");
        i++;
        lende = lendet[i];
    }
    strcat(result, "#");

    createConnection("lende.txt", 'w');
    saveToFile(result);
    closeConnection();

    isLdBufferLoaded = false;

    return true;
}

/**
 * Nqs te dhenat nuk jan ngarkuar nga skedari, i ngarkon dhe
 * kthen vektorin lendet.
 *
 * @return
 */
Lende *getLendet() {
    if (!isLdBufferLoaded) {
        loadLendet();
    }

    return lendet;
}

/**
 * Perditeson te dhenat per nje lende
 *
 * @param lende
 * @return
 */
bool updateLende(Lende lende) {
    int index = getLendeIndex(lende.id);

    if (index == -1) {
        return false;
    } else {
        strcpy(lendet[index].emer, lende.emer);
        lendet[index].klasat_ids = lende.klasat_ids;
        lendet[index].studentet_ids = lende.studentet_ids;
        lendet[index].nr_student = lende.nr_student;
    }

    saveLendet();
    return true;
}