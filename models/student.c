//
// Created by krisl on 6/4/2019.
//

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "functions.h"
#include "file.h"
#include "student.h"

char *bStudentet;                   // stringa me te dhenat e paperpunuara nga skedari
int lastStudentId;                  // ID e elementit te fundit ne vektorin e studenteve
bool isStBufferLoaded = false;      // percakton nqs e kemi lexuar skedarin apo jo

Student studentet[999];             // vektor strukture studentet

bool isStudentLoggedIn = false;     // behet true kur studentu logohet
Student sessionStudent;             // do marri vleren e studentit qe do futet ne sistem

Note *parseNotat(char *data);
bool saveStudentet();
int getStudentIndex(int ID);

struct Note *struct_array;

/**
 * Merr username dhe password nga perdoruesi dhe kerkon per nje student.
 *
 * @return true nqs gjendet sekretarja, false ne te kundert
 */
bool loginStudent() {
    isStBufferLoaded = false;

    int ID;
    char passw[50];

    printf("ID studenti: ");
    scanf("%d", &ID);

    printf("Fjalekalimi: ");
    scanf("%s", passw);
    printf("\nDuke ju identifikuar...\n");

    Student rezultat = getStudent(ID, passw);
    if (strcmp(rezultat.emer, "error") == 0) {
        printf("Nje nga te dhenat qe keni futur nuk eshte e sakte!\n");
        return false;
    } else {
        printf("Identifikimi u krye me sukses!\n");
        isStudentLoggedIn = true;
        sessionStudent = rezultat;
        return true;
    }
}

/**
 * Shton nje strukture te tipit Student ne nje indeks te caktuar te vektorit studentet.
 *
 * @param Student
 * @param index
 */
void addStudentToList(Student Student, int index) {
    studentet[index] = Student;
    lastStudentId = index;
}

/**
 * Krijon lidhjen me skedarin 'student.txt', lexon te dhenat dhe i perpunon
 * duke krijuar nje strukture te tipit Student per cdo te dhene ne skedar.
 *
 * @return bool
 */
void loadStudentet() {
    char *fStudentet;
    char *fFields;

    createConnection("student.txt", 'r');
    loadBuffer();
    closeConnection();
    bStudentet = getBuffer();

    char *file = strdup(bStudentet);
    file = strtok(file, "#");

    Student tmp;
    int field = 0;
    int count = 0;
    char klasId[20];
    while ((fStudentet = strsep(&file, "|"))) {
        if (strlen(fStudentet) > 0) {
            while ((fFields = strsep(&fStudentet, "~"))) {
                if (strlen(fFields) > 0) {
                    switch (field) {
                        case 0:
                            tmp.id = parseInt(fFields, strlen(fFields));
                            break;
                        case 1:
                            strcpy(tmp.emer, fFields);
                            break;
                        case 2:
                            strcpy(tmp.mbiemer, fFields);
                            break;
                        case 3:
                            strcpy(tmp.fjalekalim, fFields);
                            break;
                        case 4:
                            strcpy(klasId, fFields);
                            tmp.klasa = getKlase(klasId);
                            break;
                        case 5:
                            tmp.notat = parseNotat(fFields);
                            break;

                    }
                    field++;
                }
            }
            field = 0;
            addStudentToList(tmp, count);
            count++;
        }
    }

    Student last;
    strcpy(last.emer, "-1");

    addStudentToList(last, count);
    isStBufferLoaded = true;
}

/**
 * Lexon stringen e paperpunuar nga skedari me notat dhe ID-t e lendes te ndara me - ne mes,
 * i perpunon, i perkthen ne nje vektor strukturash, dhe rikthen vektorin.
 *
 * @param data
 * @return
 */
Note *parseNotat(char *data) {
    char *fFields;
    Note notat[999];

    int field = 0;
    int count = 0;
    while ((fFields = strsep(&data, "-"))) {
        if (strlen(fFields) > 0) {
            Note tmp;

            if (field % 2 == 0) {
                tmp.lenda_id = parseInt(fFields, strlen(fFields));
            } else {
                tmp.nota = parseFloat(fFields);
                notat[(field / 2)] = tmp;
                count++;
            }

            field++;
        }
    }

    struct_array = malloc(count * sizeof(Note));

    int i = 0;
    for (i = 0; i < count; i++) {
        struct_array[i] = notat[i];
    }

    Note last;
    last.lenda_id = -1;
    struct_array[i] = last;

    return struct_array;
}

/**
 * Kerkon ne vektorin studentet per studentin me ID-n dhe fjalekalimin e dhene.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku emri eshte 'error'.
 *
 * @param ID
 * @param password
 * @return
 */
Student getStudentById(int ID) {
    Student tmp;

    if (!isStBufferLoaded) {
        loadStudentet();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bStudentet) == 0) {
        strcpy(tmp.emer, "error");
        return tmp;
    }

    int i = 0;
    Student sek = studentet[i];

    // itero deri ne fund te vektorit me Student (shenuar me -1)
    while (strcmp(sek.emer, "-1") != 0) {
        // nqs gjendet Student me ID-n e dhene
        if (sek.id == ID) {
            return sek;
        }

        i++;
        sek = studentet[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje Student me kredencialet e dhena
    strcpy(tmp.emer, "error");

    return tmp;
}

/**
 * Kerkon ne vektorin studentet per studentin me ID-n dhe fjalekalimin e dhene.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku emri eshte 'error'.
 *
 * @param ID
 * @param password
 * @return
 */
Student getStudent(int ID, char *password) {
    Student tmp;

    if (!isStBufferLoaded) {
        loadStudentet();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bStudentet) == 0) {
        strcpy(tmp.emer, "error");
        return tmp;
    }

    int i = 0;
    Student sek = studentet[i];

    // itero deri ne fund te vektorit me Student (shenuar me -1)
    while (strcmp(sek.emer, "-1") != 0) {
        // nqs gjendet Student me ID-n e dhene
        if (sek.id == ID) {
            // kontrollo passwordin, nqs perputhet, kthe Studentn
            if (strcmp(sek.fjalekalim, password) == 0) {
                return sek;
            }
                // nqs nuk perputhet, passwordi gabim, kthe error
            else {
                strcpy(tmp.emer, "error");
                return tmp;
            }
        }

        i++;
        sek = studentet[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje Student me kredencialet e dhena
    strcpy(tmp.emer, "error");

    return tmp;
}

void addStudent(char *emer, char *mbiemer, char *fjalekalim, char *klasId, float *notat, int *lendet, int nrNota) {
    Student stu;
    stu.id = (int) time(NULL);
    strcpy(stu.emer, emer);
    strcpy(stu.mbiemer, mbiemer);
    strcpy(stu.fjalekalim, fjalekalim);

    if (!isStBufferLoaded) {
        loadStudentet();
    }

    if (strcmp(klasId, "-1") != 0) {
        stu.klasa = getKlase(klasId);
    } else {
        Klase null;
        strcpy(null.id, "-1");
        stu.klasa = null;
    }

    int multiply = nrNota;
    if (nrNota == 0) {
        multiply = 1;
    }

    struct_array  = malloc(multiply * sizeof(Note));
    int i;
    for (i = 0; i < nrNota; i++)  {
        Note tmp;
        tmp.lenda_id  = lendet[i];
        tmp.nota = notat[i];

        struct_array[i] = tmp;
    }

    Note last;
    last.lenda_id = -1;
    struct_array[i] = last;

    stu.notat = struct_array;

    addStudentToList(stu, lastStudentId);

    // shto elementin e fundit me -1
    strcpy(stu.emer, "-1");
    addStudentToList(stu, ++lastStudentId);
    saveStudentet();
}

/**
 * Nqs te dhenat nuk jan ngarkuar nga skedari, i ngarkon dhe
 * kthen vektorin studentet.
 *
 * @return
 */
Student *getStudentet() {
    if (!isStBufferLoaded) {
        loadStudentet();
    }

    return studentet;
}

bool saveStudentet() {
    char result[999] = "";

    int i = 0;
    Student stu = studentet[i];
    while (strcmp(stu.emer, "-1") != 0) {
        if (strcmp(stu.emer, "delete") != 0) {
            char tmp[255];
            sprintf(tmp, "%d", stu.id);
            strcat(result, tmp);
            memset(&tmp[0], 0, sizeof(tmp));
            strcat(result, "~");
            strcat(result, stu.emer);
            strcat(result, "~");
            strcat(result, stu.mbiemer);
            strcat(result, "~");
            strcat(result, stu.fjalekalim);
            strcat(result, "~");
            strcat(result, stu.klasa.id);
            strcat(result, "~");

            if (stu.notat == NULL) {
                stu.notat = malloc(sizeof(Note));
                stu.notat[0].lenda_id = -1;
            }

            // ruaj vektorin me nota ne skedar duke i ndare me "-"
            int j = 0;
            char tmpp[255];
            while (stu.notat[j].lenda_id != -1) {
                sprintf(tmpp, "%d-%.2f", stu.notat[j].lenda_id, stu.notat[j].nota);

                if (stu.notat[j + 1].lenda_id != -1) {
                    strcat(tmpp, "-");
                }

                strcat(result, tmpp);
                j++;
                memset(&tmpp[0], 0, sizeof(tmpp));
            }

            // nqs nuk ka nota, vendos -1
            if (j == 0) {
                strcat(result, "-1");
            }

            strcat(result, "|");
        }
        i++;
        stu = studentet[i];
    }
    strcat(result, "#");

    createConnection("student.txt", 'w');
    saveToFile(result);
    closeConnection();

    isStBufferLoaded = false;

    return true;
}

int getStudentIndex(int ID) {
    if (!isStBufferLoaded) {
        loadStudentet();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bStudentet) == 0) {
        return -1;
    }

    bool found = false;

    int i = 0;
    Student sek = studentet[i];

    while (strcmp(sek.emer, "-1") != 0) {
        if (sek.id == ID) {
            found = true;
            break;
        }

        i++;
        sek = studentet[i];
    }

    if (found) {
        return i;
    } else {
        return -1;
    }
}

bool updateStudent(Student student) {
    int index = getStudentIndex(student.id);

    if (index == -1) {
        return false;
    } else {
        strcpy(studentet[index].emer, student.emer);
        strcpy(studentet[index].mbiemer, student.mbiemer);
        strcpy(studentet[index].fjalekalim, student.fjalekalim);
        studentet[index].klasa = student.klasa;
        studentet[index].notat = student.notat;
    }

    saveStudentet();
    return true;
}

Note *addToNotat(Note note, Note *notat) {
    int count = 0;
    while (notat[count].lenda_id != -1) {
        count++;
    }

    notat[count] = note;
    notat[count+1].lenda_id = -1;

    return notat;
}

bool isLendePresent(Lende lende, Student student) {
    int i = 0;
    bool found = false;
    while (student.notat[i].lenda_id != -1) {
        if (lende.id == student.notat[i].lenda_id) {
            found = true;
        }
        i++;
    }

    return found;
}

void deleteStudent(int ID) {
    int indeks = getStudentIndex(ID);

    strcpy(studentet[indeks].emer, "delete");

    saveStudentet();
}

Student getSessionStudent() {
    if (isStudentLoggedIn) {
        return sessionStudent;
    } else {
        Student tmp;
        strcpy(tmp.emer, "error");

        return tmp;
    }
}