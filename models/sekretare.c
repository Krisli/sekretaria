//
// Created by krisli on 19-06-04.
//

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <file.h>
#include "sekretare.h"

char *bSekretare;                   // stringa me te dhenat e paperpunuara nga skedari
int lastId;                         // ID e elementit te fundit ne vektorin e sekretareve
bool isBufferLoaded = false;        // percakton nqs e kemi lexuar skedarin apo jo

Sekretare sekretaret[999];          // vektor strukture Sekretare

bool isLoggedIn = false;            // behet true kur sekretarja logohet
Sekretare sessionSekretare;         // do marri vleren e sekretares qe do futet ne sistem

/**
 * Merr username dhe password nga perdoruesi dhe kerkon per nje sekretare.
 *
 * @return true nqs gjendet sekretarja, false ne te kundert
 */
bool loginSekretare() {
    isBufferLoaded = false;

    char username[50];
    char passw[50];

    printf("Emer perdoruesi: ");
    scanf("%s", username);

    printf("Fjalekalimi: ");
    scanf("%s", passw);
    printf("\nDuke ju identifikuar...\n");

    Sekretare rezultat = getSekretare(username, passw);
    if (strcmp(rezultat.username, "error") == 0) {
        printf("Nje nga te dhenat qe keni futur nuk eshte e sakte!\n");
        return false;
    } else {
        printf("Identifikimi u krye me sukses!\n");
        sessionSekretare = rezultat;
        isLoggedIn = true;
        return true;
    }
}

/**
 * Shton nje strukture te tipit Sekretare ne nje indeks te caktuar te vektorit sekretaret.
 *
 * @param sekretare
 * @param index
 */
void addToList(Sekretare sekretare, int index) {
    sekretaret[index] = sekretare;
    lastId = index;
}

/**
 * Krijon lidhjen me skedarin 'sekretare.txt', lexon te dhenat dhe i perpunon
 * duke krijuar nje strukture te tipit Sekretare per cdo te dhene ne skedar.
 *
 * @return bool
 */
void loadSekretaret() {
    char *fSekretaret;
    char *fFields;

    createConnection("sekretare.txt", 'r');
    loadBuffer();
    closeConnection();
    bSekretare = getBuffer();

    char *file = strdup(bSekretare);
    file = strtok(file, "#");

    Sekretare tmp;
    int field = 0;
    int count = 0;
    while ((fSekretaret = strsep(&file, "|"))) {
        if (strlen(fSekretaret) > 0) {
            while ((fFields = strsep(&fSekretaret, "~"))) {
                if (strlen(fFields) > 0) {
                    switch (field) {
                        case 0:
                            strcpy(tmp.emer, fFields);
                            break;
                        case 1:
                            strcpy(tmp.mbiemer, fFields);
                            break;
                        case 2:
                            strcpy(tmp.username, fFields);
                            break;
                        case 3:
                            strcpy(tmp.fjalekalim, fFields);
                            break;
                    }
                    field++;
                }
            }
            field = 0;
            addToList(tmp, count);
            count++;
        }
    }

    Sekretare last;
    strcpy(last.emer, "-1");

    addToList(last, count);
    isBufferLoaded = true;
}

/**
 * Kerkon ne vektorin sekretaret per sekretaren me username dhe passwordin e dhene.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku username eshte 'error'.
 *
 * @param username
 * @param password
 * @return
 */
Sekretare getSekretare(char *username, char *password) {
    Sekretare tmp;

    if (!isBufferLoaded) {
        loadSekretaret();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bSekretare) == 0) {
        strcpy(tmp.username, "error");
        return tmp;
    }

    int i = 0;
    Sekretare sek = sekretaret[i];

    // itero deri ne fund te vektorit me sekretare (shenuar me -1)
    while (strcmp(sek.emer, "-1") != 0) {
        // nqs gjendet sekretare me username-n e dhene
        if (strcmp(sek.username, username) == 0) {
            // kontrollo passwordin, nqs perputhet, kthe sekretaren
            if (strcmp(sek.fjalekalim, password) == 0) {
                return sek;
            }
                // nqs nuk perputhet, passwordi gabim, kthe error
            else {
                strcpy(tmp.username, "error");
                return tmp;
            }
        }

        i++;
        sek = sekretaret[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje sekretare me kredencialet e dhena
    strcpy(tmp.username, "error");

    return tmp;
}

/**
 * Kerkon ne vektorin sekretaret per sekretaren me username.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku username eshte 'error'.
 *
 * @param username
 * @return
 */
Sekretare getSekretareByUsername(char *username) {
    Sekretare tmp;

    if (!isBufferLoaded) {
        loadSekretaret();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bSekretare) == 0) {
        strcpy(tmp.username, "error");
        return tmp;
    }

    int i = 0;
    Sekretare sek = sekretaret[i];

    // itero deri ne fund te vektorit me sekretare (shenuar me -1)
    while (strcmp(sek.emer, "-1") != 0) {
        // nqs gjendet sekretare me username-n e dhene
        if (strcmp(sek.username, username) == 0) {
            return sek;
        }

        i++;
        sek = sekretaret[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje sekretare me kredencialet e dhena
    strcpy(tmp.username, "error");

    return tmp;
}

/**
 * Kerkon ne vektorin sekretaret per sekretaren me ane te emrit dhe mbiemrit.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku username eshte 'error'.
 *
 * @param emer
 * @param mbiemer
 * @return
 */
Sekretare getSekretareByNameAndSurname(char *emer, char *mbiemer) {
    Sekretare tmp;

    if (!isBufferLoaded) {
        loadSekretaret();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bSekretare) == 0) {
        strcpy(tmp.username, "error");
        return tmp;
    }

    int i = 0;
    Sekretare sek = sekretaret[i];

    // itero deri ne fund te vektorit me sekretare (shenuar me -1)
    while (strcmp(sek.emer, "-1") != 0) {
        // nqs gjendet sekretare me emrin dhe mbiemrin e dhene
        if (strcmp(sek.emer, emer) == 0 && strcmp(sek.mbiemer, mbiemer) == 0) {
            return sek;
        }

        i++;
        sek = sekretaret[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje sekretare me kredencialet e dhena
    strcpy(tmp.username, "error");

    return tmp;
}

/**
 * Gjen indeksin e nje sekretareje ne vektorin e sekretareve me ane te username.
 * Nqs vektori eshte bosh ose username nuk gjendet, kthen -1.
 *
 * @param username
 * @return indeksin nqs gjendet, -1 ne te kundert
 */
int getSekretareIndex(char *username) {
    if (!isBufferLoaded) {
        loadSekretaret();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bSekretare) == 0) {
        return -1;
    }

    bool found = false;

    int i = 0;
    Sekretare sek = sekretaret[i];

    while (strcmp(sek.emer, "-1") != 0) {
        if (strcmp(sek.username, username) == 0) {
            found = true;
            break;
        }

        i++;
        sek = sekretaret[i];
    }

    if (found) {
        return i;
    } else {
        return -1;
    }

}

/**
 * Shton nje strukture te tipit Sekretare ne fund te vektorit sekretaret, bazuar ne
 * te dhenat qe i kalohen funksionit.
 *
 * @param emer
 * @param mbiemer
 * @param username
 * @param fjalekalim
 */
void addSekretare(char *emer, char *mbiemer, char *username, char *fjalekalim) {
    Sekretare sek;
    strcpy(sek.emer, emer);
    strcpy(sek.mbiemer, mbiemer);
    strcpy(sek.username, username);
    strcpy(sek.fjalekalim, fjalekalim);

    // sigurohu qe te dhenat te jene ngarkuar nga skedari
    if (!isBufferLoaded) {
        loadSekretaret();
    }

    addToList(sek, lastId);

    // shto elementin e fundit me -1
    strcpy(sek.emer, "-1");
    addToList(sek, ++lastId);

    saveSekretaret();
}

/**
 * Perkthen vektorin sekretaret ne formatin e skeadrit, dhe ruan
 * te dhenat te 'sekretare.txt'.
 *
 * @return
 */
bool saveSekretaret() {
    char result[999] = "";

    int i = 0;
    Sekretare sek = sekretaret[i];
    while (strcmp(sek.emer, "-1") != 0) {
        if (strcmp(sek.username, "delete") != 0) {
            strcat(result, sek.emer);
            strcat(result, "~");
            strcat(result, sek.mbiemer);
            strcat(result, "~");
            strcat(result, sek.username);
            strcat(result, "~");
            strcat(result, sek.fjalekalim);
            strcat(result, "|");
        }
        i++;
        sek = sekretaret[i];
    }
    strcat(result, "#");

    createConnection("sekretare.txt", 'w');
    saveToFile(result);
    closeConnection();

    isBufferLoaded = false;

    return true;
}

/**
 * Nqs te dhenat nuk jan ngarkuar nga skedari, i ngarkon dhe
 * kthen vektorin sekretaret.
 *
 * @return
 */
Sekretare *getSekretaret() {
    if (!isBufferLoaded) {
        loadSekretaret();
    }

    return sekretaret;
}

Sekretare getSessionSekretare() {
    if (isLoggedIn) {
        return sessionSekretare;
    } else {
        Sekretare tmp;
        strcpy(tmp.emer, "error");

        return tmp;
    }
}

void deleteSekretare(char *username) {
    int indeks = getSekretareIndex(username);

    strcpy(sekretaret[indeks].username, "delete");

    saveSekretaret();
    isBufferLoaded = false;
}

bool updateSekretare(Sekretare sekretare) {
    int index = getSekretareIndex(sekretare.username);

    if (index == -1) {
        return false;
    } else {
        strcpy(sekretaret[index].emer, sekretare.emer);
        strcpy(sekretaret[index].mbiemer, sekretare.mbiemer);
        strcpy(sekretaret[index].fjalekalim, sekretare.fjalekalim);
        strcpy(sekretaret[index].username, sekretare.username);
    }

    saveSekretaret();
    return true;
}