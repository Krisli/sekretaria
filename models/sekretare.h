//
// Created by krisli on 19-06-04.
//

#ifndef SEKRETARIA_SEKRETARE_H
#define SEKRETARIA_SEKRETARE_H

#include <stdbool.h>

typedef struct Sekretare {
    char emer[50];
    char mbiemer[50];
    char username[50];
    char fjalekalim[255];
} Sekretare;

bool loginSekretare();

void addSekretare(char *emer, char *mbiemer, char *username, char *fjalekalim);
bool saveSekretaret();

void loadSekretaret();
Sekretare getSekretare(char *username, char *password);
Sekretare getSekretareByUsername(char *username);
Sekretare getSekretareByNameAndSurname(char *emer, char *mbiemer);
Sekretare *getSekretaret();
int getSekretareIndex(char *username);

Sekretare getSessionSekretare();
void deleteSekretare(char *username);
bool updateSekretare(Sekretare sekretare);

#endif //SEKRETARIA_SEKRETARE_H
