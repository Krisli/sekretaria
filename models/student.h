//
// Created by krisl on 6/4/2019.
//

#ifndef SEKRETARIA_STUDENT_H
#define SEKRETARIA_STUDENT_H

#include "klase.h"
#include "lende.h"

typedef struct Note {
    int lenda_id;
    float nota;
} Note;

typedef struct Student {
    int id;
    char emer[50];
    char mbiemer[50];
    char fjalekalim[255];
    Klase klasa;
    Note *notat;
} Student;

Student getStudentById(int ID);
Student getStudent(int ID, char *password);
Student *getStudentet();
bool updateStudent(Student student);

void addStudent(char *emer, char *mbiemer, char *fjalekalim, char *klasId, float *notat, int *lendet, int nrNota);
void deleteStudent(int ID);

Note *addToNotat(Note note, Note *notat);

bool isLendePresent(Lende lende, Student student);

bool loginStudent();
Student getSessionStudent();

#endif //SEKRETARIA_STUDENT_H
