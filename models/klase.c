//
// Created by krisli on 19-06-05.
//

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <file.h>
#include <functions.h>
#include "klase.h"

char *bKlasa;                       // stringa me te dhenat e paperpunuara nga skedari
int lastKlasId;                     // ID e elementit te fundit ne vektorin e klasave
bool isKlBufferLoaded = false;      // percakton nqs e kemi lexuar skedarin apo jo

Klase klasat[999];                  // vektor strukture Klase

bool saveKlasat();

/**
 * Shton nje strukture te tipit Klase ne nje indeks te caktuar te vektorit klasat.
 *
 * @param klasa
 * @param index
 */
void addKlaseToList(Klase klasa, int index) {
    klasat[index] = klasa;
    lastKlasId = index;
}

/**
 * Krijon lidhjen me skedarin 'klasat.txt', lexon te dhenat dhe i perpunon
 * duke krijuar nje strukture te tipit Klase per cdo te dhene ne skedar.
 *
 * @return bool
 */
void loadKlasat() {
    char *fKlasat;
    char *fFields;

    createConnection("klasa.txt", 'r');
    loadBuffer();
    closeConnection();
    bKlasa = getBuffer();

    char *file = strdup(bKlasa);
    file = strtok(file, "#");

    Klase tmp;
    int field = 0;
    int count = 0;
    while ((fKlasat = strsep(&file, "|"))) {
        if (strlen(fKlasat) > 0) {
            while ((fFields = strsep(&fKlasat, "~"))) {
                if (strlen(fFields) > 0) {
                    switch (field) {
                        case 0:
                            strcpy(tmp.id, fFields);
                            break;
                        case 1:
                            strcpy(tmp.dega, fFields);
                            break;
                        case 2:
                            tmp.viti = parseInt(fFields, strlen(fFields));
                            break;
                        case 3:
                            tmp.grupi = fFields[0];
                            break;
                    }
                    field++;
                }
            }
            field = 0;
            addKlaseToList(tmp, count);
            count++;
        }
    }

    Klase last;
    strcpy(last.id, "-1");

    addKlaseToList(last, count);
    isKlBufferLoaded = true;
}

/**
 * Kerkon ne vektorin klasat per klasen me ID-n e dhene.
 * Ne rast se e gjen, kthen strukturen perkatese. Ne te kundert, kthen nje strukture
 * ku ID eshte 'error'.
 *
 * @param ID
 * @return
 */
Klase getKlase(char *ID) {
    Klase tmp;

    if (!isKlBufferLoaded) {
        loadKlasat();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bKlasa) == 0) {
        strcpy(tmp.id, "error");
        return tmp;
    }

    int i = 0;
    Klase klas = klasat[i];

    // itero deri ne fund te vektorit me klasa (shenuar me -1)
    while (strcmp(klas.id, "-1") != 0) {
        // nqs gjendet klasa me ID-n e dhene
        if (strcmp(klas.id, ID) == 0) {
            return klas;
        }

        i++;
        klas = klasat[i]; // vazhdo te elementi i rradhes
    }

    // ne kete pike, nuk eshte gjetur asnje klas me ID e dhene
    strcpy(tmp.id, "error");

    return tmp;
}

/**
 * Gjen indeksin e nje klase ne vektorin e klasave me ane te ID.
 * Nqs vektori eshte bosh ose ID nuk gjendet, kthen -1.
 *
 * @param ID
 * @return indeksin nqs gjendet, -1 ne te kundert
 */
int getKlaseIndex(char *ID) {
    if (!isKlBufferLoaded) {
        loadKlasat();
    }

    // nqs stringa buffer eshte bosh, ath ska te dhena ne skedar
    if (strlen(bKlasa) == 0) {
        return -1;
    }

    bool found = false;

    int i = 0;
    Klase klas = klasat[i];

    while (strcmp(klas.id, "-1") != 0) {
        if (strcmp(klas.id, ID) == 0) {
            found = true;
            break;
        }

        i++;
        klas = klasat[i];
    }

    if (found) {
        return i;
    } else {
        return -1;
    }
}

/**
 * Shton nje strukture te tipit Klase ne fund te vektorit klasat, bazuar ne
 * te dhenat qe i kalohen funksionit.
 *
 * @param dega
 * @param viti
 * @param grupi
 */
void addKlase(char *dega, int viti, char grupi) {
    Klase klas;

    // gjenero ID e klases bazuar tek dega,viti dhe grupi
    char ID[20];
    strcpy(ID, dega);
    char tmp[255];
    sprintf(tmp, "%d", viti);
    strcat(ID, tmp);
    memset(&tmp[0], 0, sizeof(tmp));
    sprintf(tmp, "%c", grupi);
    strcat(ID, tmp);

    strcpy(klas.id, ID);
    strcpy(klas.dega, dega);
    klas.viti = viti;
    klas.grupi = grupi;

    // sigurohu qe te dhenat te jene ngarkuar nga skedari
    if (!isKlBufferLoaded) {
        loadKlasat();
    }

    addKlaseToList(klas, lastKlasId);

    // shto elementin e fundit me -1
    strcpy(klas.id, "-1");
    addKlaseToList(klas, ++lastKlasId);

    saveKlasat();
}

/**
 * Perkthen vektorin klasat ne formatin e skedarit, dhe ruan
 * te dhenat te 'klasa.txt'.
 *
 * @return
 */
bool saveKlasat() {
    char result[999] = "";

    int i = 0;
    Klase klas = klasat[i];
    while (strcmp(klas.id, "-1") != 0) {
        char tmp[255];
        strcat(result, klas.id);
        strcat(result, "~");
        strcat(result, klas.dega);
        strcat(result, "~");
        sprintf(tmp, "%d", klas.viti);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        sprintf(tmp, "%c", klas.grupi);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "|");
        i++;
        klas = klasat[i];
    }
    strcat(result, "#");

    createConnection("klasa.txt", 'w');
    saveToFile(result);
    closeConnection();

    isKlBufferLoaded = false;

    return true;
}

/**
 * Nqs te dhenat nuk jan ngarkuar nga skedari, i ngarkon dhe
 * kthen vektorin klasat.
 *
 * @return
 */
Klase *getKlasat() {
    if (!isKlBufferLoaded) {
        loadKlasat();
    }

    return klasat;
}