//
// Created by krisli on 19-06-04.
//

#ifndef SEKRETARIA_ADMIN_H
#define SEKRETARIA_ADMIN_H

#include <stdbool.h>

typedef struct Admin {
    char emer[50];
    char fjalekalim[255];
} Admin;

bool loginAdmin();

#endif //SEKRETARIA_ADMIN_H
