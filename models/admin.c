//
// Created by krisli on 19-06-04.
//

#include "admin.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <file.h>
#include <memory.h>
#include <malloc.h>
#include <functions.h>

Admin sessionAdmin;

Admin getAdmin(char *emer, char *password);

bool loginAdmin() {
    char emer[255];
    char passw[255];

    printf("Emri: ");
    scanf("%s", emer);

    printf("Fjalekalimi: ");
    scanf("%s", passw);
    printf("\nDuke ju identifikuar...\n");

    Admin rezultat = getAdmin(emer, passw);
    if (strcmp(rezultat.emer, "error") == 0) {
        printf("Nje nga te dhenat qe keni futur nuk eshte e sakte!\n");
        return false;
    } else {
        printf("Identifikimi u krye me sukses!\n");
        return true;
    }
}

Admin getAdmin(char *emer,char *password) {
    Admin tmp;

    createConnection("admin.txt", 'r');
    loadBuffer();
    closeConnection();

    char *buffer = getBuffer();
    char *file = strdup(buffer);
    file = strtok(file, "|");

    char *fFields;
    int field = 0;
    while ((fFields = strsep(&file, "~"))) {
        if (strlen(fFields) > 0) {
            switch (field) {
                case 0:
                    strcpy(tmp.emer, fFields);
                    break;
                case 1:
                    strcpy(tmp.fjalekalim, fFields);
                    break;
            }
        }
        field ++;
    }

    // nese emri ose fjalekalimi jane te pasakte, vendose emrin 'error' qe te kuptojme qe ka gabim
    if (strcmp(password, tmp.fjalekalim) != 0 || strcmp(emer, tmp.emer) != 0) {
        strcpy(tmp.emer, "error");
    }

    return tmp;
}